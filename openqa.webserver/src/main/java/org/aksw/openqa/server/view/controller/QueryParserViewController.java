package org.aksw.openqa.server.view.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.aksw.openqa.component.providers.impl.QueryParserProvider;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
@ManagedBean(name="inputInterpretationViewController")
@ViewScoped
public class QueryParserViewController extends ComponentViewController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2260026837025370659L;

	public QueryParserViewController() {
		super(QueryParserProvider.class);
	}
}