package org.aksw.openqa.server;

import java.util.Map;

import org.aksw.openqa.Properties;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.param.ResultMap;
import org.aksw.openqa.component.param.ResultMapList;
import org.aksw.openqa.component.service.cache.impl.Cache;
import org.aksw.openqa.server.servlet.SearchServlet;
import org.aksw.openqa.server.util.ResultUtil;
import org.aksw.openqa.util.PropertyLoaderUtil;
import org.apache.log4j.Logger;
import org.json.JSONArray;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class PreDefinedQueries {
	
	private static Logger logger = Logger.getLogger(PreDefinedQueries.class);
	
	public static final String PRE_DEFINED_QUERY_FILE = "def-queries.list";

	private static PreDefinedQueries preDefinedQueries = null;
	private Cache<String, IResultMapList<? extends IResultMap>> resourceCache;
	private Cache<String, IResultMapList<? extends IResultMap>> queryCache;
	
	public PreDefinedQueries() {
		try {
			String appContextDir = (String) ServerEnviromentVariables.getInstance().
					getParam(ServerEnviromentVariables.CONTEXT_DIR);
			Map<String, Object> properties = PropertyLoaderUtil.getPropertiesWithWhiteSpaceFromFile(appContextDir + "/" + PRE_DEFINED_QUERY_FILE);
			if(properties != null) {
				resourceCache = new Cache<String, IResultMapList<? extends IResultMap>>(properties.size());
				queryCache = new Cache<String, IResultMapList<? extends IResultMap>>(properties.size());
				for(String key :  properties.keySet()) {
					String property = properties.get(key).toString();
					String[] params = property.split(",");
					
					// parse resource
					JSONArray jsonObject = new JSONArray(params[0]);
					ResultMapList<IResultMap> resultList = ResultUtil.toResult(jsonObject);
					key = key.toLowerCase();
					resourceCache.put(key, resultList);
					
					// query
					resultList = new ResultMapList<IResultMap>();
					ResultMap result = new ResultMap();
					result.setParam(Properties.SPARQL, params[1].trim());
					resultList.add(result);
					queryCache.put(key, resultList);
				}
			}
		} catch (Exception e) {
			logger.error("Error initializing pre-defined queries", e);
		}
	}
	
	public synchronized static PreDefinedQueries getInstance() {
		if(preDefinedQueries == null) {
			preDefinedQueries = new PreDefinedQueries();
		}		
		return preDefinedQueries;
	}

	public synchronized IResultMapList<? extends IResultMap> getQuery(String query, String content) {
		query = query.toLowerCase(); // lower casing query
		if(content != null && content.equals(SearchServlet.OUTPUT_CONTENT_PARAM_TYPE_SPARQL)) {			
			return queryCache.get(query);
		}
		return resourceCache.get(query);
	}
}
