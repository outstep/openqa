package org.aksw.openqa.server.servlet.listener;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.service.cache.impl.CacheService;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.aksw.openqa.manager.plugin.PluginManagerPool;
import org.aksw.openqa.server.PluginManagerPoolInstanciator;
import org.aksw.openqa.server.ServerEnviromentVariables;
import org.aksw.openqa.server.manager.log.ConsoleAppender;
import org.aksw.openqa.server.manager.log.ConsoleLog;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class SearchServletContextListener implements javax.servlet.ServletContextListener {
 
	private static Logger logger = Logger.getLogger(SearchServletContextListener.class);
	
    public void contextInitialized(ServletContextEvent event) {
    	logger.info("openQA is being initialized");

    	ConsoleAppender appender = new ConsoleAppender();
    	Logger.getRootLogger().addAppender(appender);

		ServletContext context = event.getServletContext();
		String contextPath = context.getRealPath(File.separator);
		ClassLoader contextClassLoader = context.getClassLoader();
		String appContext = "webserver";
    	PluginManager plugginManager = new PluginManager(contextPath + "/plugins", contextClassLoader, appContext);
    	CacheService cacheService = plugginManager.getPlugin(ServiceProvider.class, CacheService.class);
    	PluginManagerPool pool = new PluginManagerPool(contextPath + "/plugins", contextClassLoader, appContext, cacheService);
    	PluginManagerPoolInstanciator poolInstanciator = new PluginManagerPoolInstanciator(contextPath + "/plugins", contextClassLoader, appContext, cacheService);
    	
    	ServerEnviromentVariables.getInstance().setParam(ServerEnviromentVariables.PLUGIN_MANAGER, plugginManager);
    	ServerEnviromentVariables.getInstance().setParam(ServerEnviromentVariables.PLUGGIN_DIR, contextPath + "/plugins");
    	ServerEnviromentVariables.getInstance().setParam(ServerEnviromentVariables.CONTEXT_DIR, contextPath + "context");
    	ServerEnviromentVariables.getInstance().setParam(ServerEnviromentVariables.ClASS_LOADER, contextClassLoader);
    	ServerEnviromentVariables.getInstance().setParam(ServerEnviromentVariables.APP_CONTEXT, appContext);
    	ServerEnviromentVariables.getInstance().setParam(ServerEnviromentVariables.PLUGIN_MANAGER_POOL, pool);
    	ServerEnviromentVariables.getInstance().setParam(ServerEnviromentVariables.PLUGIN_MANAGER_POOL_INSTANCIATOR, poolInstanciator);

        logger.info("openQA is initialized");
    }
 
    public void contextDestroyed(ServletContextEvent event) {
    	logger.info("openQA is being shutting down");
    	PluginManager pluginManager = ServerEnviromentVariables.getPluginManager();
    	if(pluginManager != null) {
    		pluginManager.setActive(false);
    	}    	
    	PluginManagerPool pool = (PluginManagerPool) ServerEnviromentVariables.getInstance().getParam(ServerEnviromentVariables.PLUGIN_MANAGER_POOL);
    	if(pool != null) {
    		pool.close();
    	}
    	ConsoleLog.getInstance().shutdown();
    }
}