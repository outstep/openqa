package org.aksw.openqa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.component.IComponent;
import org.aksw.openqa.component.IProcess;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.context.impl.Context;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.param.ParamMap;
import org.aksw.openqa.component.providers.impl.ContextProvider;
import org.aksw.openqa.component.providers.impl.QueryParserProvider;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.providers.impl.SynthesizerProvider;
import org.aksw.openqa.main.ProcessResult;
import org.aksw.openqa.main.QueryResult;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class AnswerFormulation implements IComponent {
	
	private static Logger logger = Logger.getLogger(AnswerFormulation.class);
	
    public static List<Stage> DEFAULT_PIPELINE = Collections.synchronizedList(new DefaultPipeline());
    
    private PluginManager pluginManager;
	
	public AnswerFormulation() {
	}
	
	public AnswerFormulation(PluginManager pluginManager) {
		this.pluginManager = pluginManager;
	}
	
	public QueryResult process(String query) throws Exception {
		return process(query, pluginManager);
	}
	
	public QueryResult process(String query, PluginManager pluginManager) throws Exception {
		logger.info("Processing query " + query);
		List<IParamMap> params = new ArrayList<IParamMap>();
		ParamMap param = new ParamMap();
		param.setParam(Properties.Literal.TEXT, query);
		params.add(param);
		QueryResult queryResult = process(params, pluginManager);
		logger.debug("Answer formulation Runtime(ms) " + queryResult.getRuntime());
		return queryResult;
	}
	
	public QueryResult process(String query, PluginManager pluginManager, List<Stage> pipeline) throws Exception {
		logger.info("Processing query " + query);
		List<IParamMap> params = new ArrayList<IParamMap>();
		ParamMap param = new ParamMap();
		param.setParam(Properties.Literal.TEXT, query);
		params.add(param);
		Date start = new Date();
		QueryResult queryResult = process(params, pluginManager, pipeline); 
		Date end = new Date();		
		queryResult.setRuntime(end.getTime() - start.getTime());
		logger.debug("Answer formulation Runtime(ms) " + queryResult.getRuntime());
		return queryResult;
	}

	public QueryResult process(List<IParamMap> params,
			PluginManager pluginManager, List<Stage> stages) {
		ServiceProvider services = pluginManager.getProvider(ServiceProvider.class);
		ContextProvider contextProvider = pluginManager.getProvider(ContextProvider.class);
		IContext context = contextProvider.get(IContext.class);
		
		// setting up the context
		if(context != null) {
			context.setParam(IContext.REQUEST_HOST, "localhost");
		}
		
		return process(params, services,  context, pluginManager, stages);
	}
	
	public QueryResult process(List<? extends IParamMap> params, ServiceProvider services, IContext context, PluginManager pluginManager, List<Stage> stages) {
		return process(params, services, context, pluginManager, stages.toArray(new Stage[stages.size()]));
	}
	
	public QueryResult process(List<? extends IParamMap> params, ServiceProvider services, IContext context, PluginManager pluginManager, Stage... stages) {
		List<? extends IParamMap> inputParams = params;
		Date start = new Date();
		QueryResult queryResult = new QueryResult();
		queryResult.setSource(this);
		queryResult.setInput(params);
		
		IResultMapList<? extends IResultMap> results = null;
		for(Stage stage : stages) {
			results = stage.process(inputParams, services, context, pluginManager);			
			inputParams = results; 
			queryResult.setParam(stage.getId(), results);
		}
		queryResult.setOutput(results);
		Date end = new Date();
		queryResult.setRuntime(start.getTime() - end.getTime());
		return queryResult;
	}
	
	protected ProcessResult executeAFProcess(List<? extends IParamMap> params, 
			IProcess process, ServiceProvider services, 
			IContext context) {		
		Date start = new Date();
		Throwable throwable = null;
		IResultMapList<? extends IResultMap> results = null;
		try {
			results = process.process(params, services, context);
		} catch (Throwable t) {
			throwable = t;
		}
		Date end = new Date();		
		ProcessResult processResult = new ProcessResult();
		processResult.setOutput(results);
		processResult.setRuntime(end.getTime() - start.getTime()); // setting the runtime of the process
		processResult.setInput(params);
		processResult.setException(throwable);
		
		return processResult;
	}

	private QueryResult process(List<IParamMap> params,
			PluginManager pluginManager) throws Exception {
		Date start = new Date();
		ContextProvider contextProvider = pluginManager.getProvider(ContextProvider.class);
		ServiceProvider serviceProvider = pluginManager.getProvider(ServiceProvider.class);
		QueryParserProvider interpreterProvider = pluginManager.getProvider(QueryParserProvider.class);
		RetrieverProvider retrieverProvider = pluginManager.getProvider(RetrieverProvider.class);
		SynthesizerProvider synthesizerProvider = pluginManager.getProvider(SynthesizerProvider.class);
		
		IContext context = contextProvider.get(IContext.class);
		
		// setting up the context
		if(context != null) {
			context.setParam(IContext.REQUEST_HOST, "localhost");
		}
		
		QueryResult result = new QueryResult();
		result.setComponentSource(this);
		result.setInput(params);

		logger.debug("Interpreting");
		// Interpreting
		ProcessResult processResult = executeAFProcess(params, interpreterProvider, serviceProvider, context);
		IResultMapList<? extends IResultMap> interpretations = processResult.getOutput();
		logger.debug("Number of interpretations: " + interpretations.size());
		logger.debug("Interpreting runtime: " + processResult.getRuntime());
		// set Query Parsing result
		result.setParam(QueryResult.Attr.QUERYPARSING_RESULT, processResult);

		logger.debug("Retrieving");
		// Retrieving
		processResult = executeAFProcess(interpretations, retrieverProvider, serviceProvider, context);
		IResultMapList<? extends IResultMap> retrievingResults = processResult.getOutput();
		logger.debug("Number of retrieved results: " + retrievingResults.size());
		logger.debug("Retrieval runtime: " + processResult.getRuntime());
		// set Retrieving result
		result.setParam(QueryResult.Attr.RETRIEVAL_RESULT, processResult);

		logger.debug("Synthesizing");
		// Synthesizing
		processResult = executeAFProcess(retrievingResults, synthesizerProvider, serviceProvider, context);
		IResultMapList<? extends IResultMap> synthesisResults = processResult.getOutput();
		logger.debug("Number of synthesis: " + synthesisResults.size());
		logger.debug("Synthesis runtime: " + processResult.getRuntime());
		// set Synthesizer result
		result.setParam(QueryResult.Attr.SYNTHESIS_RESULT, processResult);

		result.setOutput(synthesisResults);
		
		Date end = new Date();
		result.setRuntime(end.getTime() - start.getTime());
		return result;
	}
	
	public QueryResult process(
			Map<String, Object> params,
			PluginManager pluginManager, 
			Context context		
			) {
			List<Stage> stages = AnswerFormulation.DEFAULT_PIPELINE;
			ServiceProvider services = (ServiceProvider) pluginManager.getProvider(ServiceProvider.class);
			ParamMap paramMap = new ParamMap();

			logger.info("Parameterizing");		
			// param set
			for(Entry<String, Object> entry : params.entrySet()) {
				paramMap.setParam(entry.getKey(), entry.getValue());
			}

			List<IParamMap> paramMaps = new ArrayList<IParamMap>();
			paramMaps.add(paramMap);

		return process(paramMaps, services, context, pluginManager, stages);
	}

	public QueryResult process(
			Map<String, Object> params,
			PluginManager pluginManager, 
			Context context,
			List<Stage> stages			
			) {
			ServiceProvider services = (ServiceProvider) pluginManager.getProvider(ServiceProvider.class);
			ParamMap paramMap = new ParamMap();
			
			logger.info("Parameterizing");		
			// param set
			for(Entry<String, Object> entry : params.entrySet()) {
				paramMap.setParam(entry.getKey(), entry.getValue());
			}
			
			List<IParamMap> paramMaps = new ArrayList<IParamMap>();
			paramMaps.add(paramMap);
			
		return process(paramMaps, services, context, pluginManager, stages);
	}
	
	@Override
	public String getId() {
		return "AnswerFormulation";
	}
}
