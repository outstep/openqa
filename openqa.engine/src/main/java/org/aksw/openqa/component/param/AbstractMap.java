package org.aksw.openqa.component.param;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractMap implements IMap {
	
	private Map<String, Object> params = new HashMap<String, Object>();

	public boolean contains(String propertyId) {
		return params.containsKey(propertyId);
	}
	
	public boolean contains(String propertyId, Class<?> clazz) {
		return ((params.containsKey(propertyId)) && (getParam(propertyId, clazz) != null));
	}
	
	public Object getParam(String propertyId) {
		return params.get(propertyId);
	}
	
	public Object getParam(String propertyId, Object defaultValue) {
		Object value = getParam(propertyId);
		if(value == null) {
			return defaultValue;
		}
		return value;
	}
	
	public <T> T getParam(String propertyId, Class<T> clazz) {
		Object property = params.get(propertyId);
		if(property != null && clazz.isAssignableFrom(property.getClass())) {
			T tProperty = clazz.cast(property);
			return tProperty;
		} else if(String.class.isInstance(property) && Number.class.isAssignableFrom(clazz)) {
			try {
				Constructor<T> f = clazz.getConstructor(property.getClass());
				return f.newInstance(property);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public <T> T getParam(String propertyId, Class<T> clazz, T defaultValue) {
		T property = getParam(propertyId, clazz);
		if(property == null) {
			return defaultValue;
		}
		return property;
	}
	
	public void setParam(String propertyId, Object o) {
		params.put(propertyId, o);
	}
	
	public void setProperties(Map<String, Object> entries) {
		if(entries != null) {
			params.putAll(entries);
		}
	}
	
	public Map<String, Object> getParameters() {
		return params;
	}
}
