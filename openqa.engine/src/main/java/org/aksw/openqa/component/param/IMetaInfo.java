package org.aksw.openqa.component.param;


public interface IMetaInfo {

	/**
	 * Return the object as a JSON object serialized as String.
	 * 
	 * @return the object as a JSON object serialized as String.
	 */
	public String toJSON();
	
	/**
	 * Print a stack trace of the process.
	 */
	public void printStackTrace();	
	
	/**
	 * 
	 * @param visitor
	 */
	public void accept(IMapVisitor visitor);

}
