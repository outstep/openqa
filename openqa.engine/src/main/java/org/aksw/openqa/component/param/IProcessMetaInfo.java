package org.aksw.openqa.component.param;

import org.aksw.openqa.component.IComponent;

public interface IProcessMetaInfo extends IMetaInfo {
	
	/**
	 * Return the input params used to generate the result.
	 * 
	 * @return the input params used to generate the result.
	 */
	public IParamMap getInputParam();
	
	/**
	 * Return the Component that generate the result.
	 * 
	 * @return the Component that generate the result.
	 */
	public IComponent getSource();
	

}
