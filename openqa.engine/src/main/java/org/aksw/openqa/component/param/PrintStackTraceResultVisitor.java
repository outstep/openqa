package org.aksw.openqa.component.param;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.aksw.openqa.component.IComponent;

public class PrintStackTraceResultVisitor implements IMapVisitor {
	
	String result = "";

	@Override
	public void visit(IParamMap params) {
		Map<String, Object> sourceParams = params.getParameters();		
		if (sourceParams != null) {
			result += "  Input Params: \n";
			for (Entry<String, Object> param : sourceParams.entrySet()) {
				System.out.println("     " + param.getKey() + " : "
						+ param.getValue());
			}
		}
	}

	@Override
	public void visit(IResultMetaInfo result) {
		IParamMap params = result.getInputParam();
		IComponent source = result.getSource();
		
		if (params != null) {
			params.accept(this);
		}
		
		if (source != null) {
			this.result += "Component: " + source.getId() + "\n";
		}
		
		if(params != null) {
			Map<String, Object> sourceParams = params.getParameters();
			if (sourceParams != null) {
				this.result += "  Input Params:" + "\n";
				for (Entry<String, Object> param : sourceParams.entrySet()) {
					this.result += "     " + param.getKey() + " : "
							+ param.getValue();
				}
			}
		}

		this.result += "  Result:\n";
		Map<String, Object> sourceParams = result.getParameters();
		if (sourceParams != null) {
			this.result += "    Output Params:\n";
			for (Entry<String, Object> param : sourceParams.entrySet()) {
				this.result += "     " + param.getKey() + " : " + param.getValue() + "\n";
			}
		}
	}

	@Override
	public void visit(IResultMap result) {
		this.result += "Result: \n" ;
		Map<String, Object> sourceParams = result.getParameters();
		if (sourceParams != null) {
			this.result += "Params:\n";
			for (Entry<String, Object> param : sourceParams.entrySet()) {
				this.result += "   " + param.getKey() + " : " + param.getValue() + "\n";
			}
		}
	}

	@Override
	public void visit(IResultMapListMetaInfo<? extends IResultMap> resultList) {
		IParamMap params = resultList.getInputParam();
		List<? extends IParamMap> paramsList = resultList.getInputParams();
		IComponent source = resultList.getSource();
		
		if (source != null) {
			this.result += "Component: " + source.getId() + "\n";
		}
		
		if (params != null) {
			params.accept(this);
		} else if(paramsList != null) {
			this.result += " Input Params List:\n";
			for(IParamMap p : paramsList) {
				if(p != null) {
					Map<String, Object> sourceParams = p.getParameters();
					if (sourceParams != null) {
						this.result += "  Input Params (" +paramsList.indexOf(p) + "):\n";
						for (Entry<String, Object> param : sourceParams.entrySet()) {
							this.result += "     " + param.getKey() + " : "
									+ param.getValue() + "\n";
						}
					}
				}
			}
		}
		
		this.result += " Result List:\n";		
		for(IResultMap r : resultList) {
			this.result += "  Result (" + resultList.indexOf(r) + ")\n";
			
			Map<String, Object> sourceParams = r.getParameters();
			if (sourceParams != null) {
				this.result += "    Output Params:\n";
				for (Entry<String, Object> param : sourceParams.entrySet()) {
					this.result += "     " + param.getKey() + " : " + param.getValue() + "\n";
				}
			}
		}
	}

	@Override
	public void visit(IResultMapList<? extends IResultMap> params) {
		for(IResultMap r : params) {
			r.accept(this);
		}
	}

	public String getResult() {
		return result;
	}
}
