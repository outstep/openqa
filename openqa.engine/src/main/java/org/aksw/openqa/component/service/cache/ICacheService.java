package org.aksw.openqa.component.service.cache;

import org.aksw.openqa.component.service.IService;
import org.aksw.openqa.component.service.cache.impl.Cache;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public interface ICacheService extends IService {
	
	public void put(String cacheContext, String key, Object value) throws Exception;
	
	public Cache<String, Object> get(String cacheContext) throws Exception;
	
	public Object get(String cacheContext, String key) throws Exception;
	
	public <T extends Object> T get(String cacheContext, String key, Class<T> clazz) throws Exception;

}
