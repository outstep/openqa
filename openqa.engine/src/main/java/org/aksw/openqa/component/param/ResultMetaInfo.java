package org.aksw.openqa.component.param;

import java.util.Map;

import org.aksw.openqa.component.IComponent;

public class ResultMetaInfo extends AbstractResult implements IResultMetaInfo {
		
	protected IComponent source;
	protected IParamMap inputParam;

	public ResultMetaInfo() {
	}
	
	public ResultMetaInfo(Map<String, Object> params, IParamMap inputParam, IComponent source) {
		// setting the source
		this.source = source;
		this.inputParam = inputParam;
		setProperties(params);
	}
	
	public ResultMetaInfo(Map<String, Object> params) {
		// setting the source
		setProperties(params);
	}
	
	public ResultMetaInfo(IParamMap inputParam, IComponent source) {
		// setting the source
		this.source = source;
		this.inputParam = inputParam;
	}

	public IComponent getSource() {
		return source;
	}
	
	public void setSource(IComponent source) {
		this.source = source;
	}

	public IParamMap getInputParam() {
		return inputParam;
	}
	
	public void setInputParam(IParamMap inputParam) {
		this.inputParam = inputParam;
	}
	
	@Override
	public void accept(IMapVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public void printStackTrace() {
		PrintStackTraceResultVisitor ps = new PrintStackTraceResultVisitor();
		ps.visit(this);
	}
	
}
