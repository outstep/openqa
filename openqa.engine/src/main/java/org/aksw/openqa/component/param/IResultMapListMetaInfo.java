package org.aksw.openqa.component.param;

import java.util.List;

public interface IResultMapListMetaInfo<E extends IResultMap> extends IResultMapList<E>, IProcessMetaInfo {
	/**
	 * Return the input params used to generate the result.
	 * 
	 * @return the input params used to generate the result.
	 */
	public List<? extends IParamMap> getInputParams();
}

