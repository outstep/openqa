package org.aksw.openqa.component;

import java.util.ArrayList;
import java.util.List;

import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.param.ResultMapList;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 * @param <C>
 * @param <F>
 */
public abstract class AbstractPluginProcessProvider<C extends IPluginProcess, F extends IPluginFactorySpi<C>> extends AbstractPluginProvider<C, F> implements IProcess {
	
	private static Logger logger = Logger.getLogger(AbstractPluginProcessProvider.class);
	
	public AbstractPluginProcessProvider(Class<F> clazz, List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(clazz, classLoaders, serviceProvider);
	}
	
	public AbstractPluginProcessProvider() {
	}
	
	protected IResultMapList<IResultMap> process(List<? extends IParamMap> arguments, List<C> components, ServiceProvider serviceProvider, IContext context) {
    	IResultMapList<IResultMap> results = new ResultMapList<IResultMap>();
        for (C component : components) {
        	// if the component is activated and can process the argument
        	List<IParamMap> canProcessArguments = getArguments(arguments, component);
			if(component.isActive() && canProcessArguments != null) {
				try {
					results.addAll(component.process(canProcessArguments, serviceProvider, context)); // process the token
				} catch (Exception e) {
					logger.error("ProcessProvider exception", e);
				} catch (Error e) {
					logger.error("ProcessProvider error", e);
				}
			}
		}
        return results;
    }
	
	public List<IParamMap> getArguments(List<? extends IParamMap> arguments, C component) {
		List<IParamMap> componentArguments = null;
		for(IParamMap argument: arguments) {
			if(component.canProcess(argument)) {
				if(componentArguments == null) {
					componentArguments = new ArrayList<IParamMap>();
				}
				componentArguments.add(argument);
			}
		}
		return componentArguments;
	}
	
	public IResultMapList<IResultMap> process(List<? extends IParamMap> arguments, ServiceProvider services, IContext context) throws Exception {
		return process(arguments, list(), services, context);
	}
}
