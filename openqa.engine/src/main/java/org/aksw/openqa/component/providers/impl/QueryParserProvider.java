package org.aksw.openqa.component.providers.impl;
import java.util.List;

import org.aksw.openqa.component.AbstractPluginProcessProvider;
import org.aksw.openqa.component.answerformulation.IQueryParser;
import org.aksw.openqa.component.answerformulation.IQueryParserFactory;
 
public class QueryParserProvider extends AbstractPluginProcessProvider<IQueryParser, IQueryParserFactory> {
    public QueryParserProvider(List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(IQueryParserFactory.class, classLoaders, serviceProvider);
	}

	@Override
	public String getId() {
		return "QueryParserProvider";
	}
}