package org.aksw.openqa.manager.plugin;

import org.aksw.openqa.component.IPlugin;
import org.apache.commons.pool2.impl.GenericObjectPool;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class PluginManagerPool extends GenericObjectPool<PluginManager> {
	
	public PluginManagerPool() {
		super(new PluginManagerPooledFactory());
	}
	
	public PluginManagerPool(String plugginDir, ClassLoader classLoader, String appContext, IPlugin... pluggins) {
		super(new PluginManagerPooledFactory(plugginDir, classLoader, appContext, pluggins));		
	}
	
	public PluginManagerPool(String plugginDir, ClassLoader classLoader) {
		super(new PluginManagerPooledFactory(plugginDir, classLoader));		
	}
}
