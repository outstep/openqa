package org.aksw.openqa.manager.plugin;

import org.aksw.openqa.component.IPlugin;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class PluginManagerPooledFactory extends BasePooledObjectFactory<PluginManager> {
		
	private IPlugin[] plugins;
	private String pluginDir;
	private ClassLoader classLoader;
	private String appContext;
		
	public PluginManagerPooledFactory() {
	}
	
	public PluginManagerPooledFactory(String pluginDir, ClassLoader classLoader, String appContext, IPlugin... plugins) {
		this.pluginDir = pluginDir;
		this.classLoader = classLoader;
		this.appContext = appContext;
		this.plugins = plugins;
	}
	
	public PluginManagerPooledFactory(String pluginDir, ClassLoader classLoader) {
		this.pluginDir = pluginDir;
		this.classLoader = classLoader;
	}
	
	@Override
	public PluginManager create() throws Exception {
		PluginManager plugginManager = null;
		if(pluginDir != null && appContext != null && plugins != null) {
			plugginManager = new PluginManager(pluginDir, classLoader, appContext, plugins);
		} else  if (pluginDir != null && appContext == null && plugins == null) {
			plugginManager = new PluginManager(pluginDir, classLoader);
		} else  {
			plugginManager = new PluginManager();
		}
		return plugginManager;
	}
	
	@Override
	public void destroyObject(PooledObject<PluginManager> p) throws Exception {
		PluginManager manager = p.getObject();
		if(manager != null) {
			manager.shutdown();
		}
	}

	@Override
	public PooledObject<PluginManager> wrap(PluginManager plugin) {
		return new DefaultPooledObject<PluginManager>(plugin);
	}
}