package org.aksw.openqa;

import java.util.ArrayList;
import java.util.List;

public class DefaultPipeline extends ArrayList<Stage> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4387911617483672688L;

	public DefaultPipeline() {
		Stage stage0 = new SequentialStage();
		List<String> componentIDs = new ArrayList<String>();
		stage0.setId("QueryParser");
		componentIDs.add("QueryParserProvider");
		stage0.setComponentIDs(componentIDs);		
		add(stage0);
		
		Stage stage1 = new SequentialStage();
		stage1.setId("Retriever");
		componentIDs = new ArrayList<String>();
		componentIDs.add("RetrieverProvider");		
		stage1.setComponentIDs(componentIDs);
		add(stage1);
		
		Stage stage2 = new SequentialStage();
		stage2.setId("Synthesizer");
		componentIDs = new ArrayList<String>();
		componentIDs.add("SynthesizerProvider");		
		stage2.setComponentIDs(componentIDs);
		add(stage2);
	}

}
