package org.aksw.openqa.component.answerformulation.retriever.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.aksw.openqa.Properties;
import org.aksw.openqa.commons.util.SPARQLUtil;
import org.aksw.openqa.component.answerformulation.AbstractRetriever;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.ResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.retriever.IResultEntryTriplestoreRetriever;
import org.aksw.openqa.component.retriever.IRetrieverEntryInstanciator;
import org.aksw.openqa.component.retriever.ResultEntry;
import org.aksw.openqa.main.OpenQA;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class TripleStoreRetriever extends AbstractRetriever implements IResultEntryTriplestoreRetriever {
	
	private static Logger logger = Logger.getLogger(TripleStoreRetriever.class);
	
	// token 
	public static final String END_POINT_PARAM = "END_POINT";
	public static final String GRAPH_PARAM = "GRAPH";
	
	public static final String READER_TIMEOUT_PARAM = "READER_TIMEOUT";
	public static final String CONNECTION_TIMEOUT_PARAM = "CONNECTION_TIMEOUT";
	
	public TripleStoreRetriever(Map<String, Object> params) {
		super(params);
	}

	@Override
	public boolean canProcess(IParamMap params) {
		return params.contains(Properties.SPARQL);
	}

	@Override
	public List<? extends IResultMap> process(IParamMap params, 
			ServiceProvider serviceProvider, 
			IContext context) {
		String sparqlQuery = params.getParam(Properties.SPARQL, String.class);
		String graph = params.getParam(GRAPH_PARAM, String.class, getParam(GRAPH_PARAM, String.class));
		String endPoint = params.getParam(END_POINT_PARAM, String.class, getParam(END_POINT_PARAM, String.class));
		
		String lowerCaseQuery = sparqlQuery.toLowerCase();
		
		/**
		 * TODO: remove the code bellow as soon as the Interpreter became Query Analyzer and 
		 * start to offer support to multi-analyzers and different pipelines.
		 */
		// if there is no ASK and LIMIT placed in query
		if(!lowerCaseQuery.contains("ask") && 
				!lowerCaseQuery.contains("limit")) {
			sparqlQuery= sparqlQuery + " limit 10 "; // add LIMIT
		}
		
		Query query = QueryFactory.create(sparqlQuery);
		boolean isQueryStar = SPARQLUtil.isSingleEntity(query);
		if(!isQueryStar) {
			EntryInstanciator resultInstanciator = new EntryInstanciator();
			return retrieve(sparqlQuery, graph, endPoint, resultInstanciator);
		} else {
			ElementGroup elementGroup  = (ElementGroup) query.getQueryPattern();
			ElementPathBlock elementTripleBlock = (ElementPathBlock) elementGroup.getElements().get(0);
			String value = elementTripleBlock.getPattern().get(0).getSubject().toString();
			ResultMap result = new ResultMap(); // setting the input parameters and source component
			result.setParam(Properties.URI, value);
			List<ResultMap> results = new ArrayList<ResultMap>();
			results.add(result);
			return results;
		}
	}	

	@Override
	public <T> List<T> retrieve(String query, 
			IRetrieverEntryInstanciator<T, ResultEntry> instanciator) {		
		String graph = getParam(GRAPH_PARAM, String.class);
		String endpoint = getParam(END_POINT_PARAM, String.class);
		return retrieve(query, 
				graph, 
				endpoint, 
				instanciator);
	}
	
	@Override
	public <T> List<T> retrieve(String query,
			String graph,
			String endpoint,
			IRetrieverEntryInstanciator<T, ResultEntry> instanciator) {
		long readTimeout = getParam(READER_TIMEOUT_PARAM, Long.class);
		long connectionTimeout = getParam(CONNECTION_TIMEOUT_PARAM, Long.class);
		return retrieve(query,
				graph,
				endpoint,
				readTimeout,
				connectionTimeout,
				instanciator);
	}

	@Override
	public <T> List<T> retrieve(String query,
			String graph,
			String endpoint,
			long connectionTimeOut,
			long readTimeOut,
			IRetrieverEntryInstanciator<T, ResultEntry> instanciator) {
		logger.debug("Processing query " + query);
		QueryEngineHTTP qexec = new QueryEngineHTTP(endpoint, query);
		qexec.setTimeout(readTimeOut, connectionTimeOut);
		qexec.addDefaultGraph(graph);
		List<T> results = new ArrayList<T>();
		try {
			ResultSet rs = qexec.execSelect();
			List<String> headers = rs.getResultVars();			
			while(rs.hasNext()) {
		    	QuerySolution solution = rs.nextSolution();
		    	// setting the input parameters and source component
		    	ResultEntry resultEntry = new ResultEntry();
		    	for(String p : headers) {
					RDFNode node = solution.get(p);					
					if(node.isResource()) {
						String value = node.toString();
						resultEntry.addProperty(p, new URI(value));
					} else if (node.isLiteral()) {
						try {
							Object value = node.asLiteral().getValue();
							resultEntry.addProperty(p, value);
						} catch (Exception e) {
							logger.warn("Object instanciating problem: " + node.toString(),  e);
						}
					}
	        	}
				T resultEncoded = instanciator.newInstance(resultEntry);
				results.add(resultEncoded);
				if(instanciator.stop()) {
					break;
				}
			}
		} catch(Exception e) {
			logger.error("Error executing query: " + query,  e);
		} finally {
			qexec.close();
		}
		return results;
	}
	
	@Override
	public String getVersion() {
		return OpenQA.ENGINE_VERSION;
	}
	
	@Override
	public String getAPI() {
		return OpenQA.API_VERSION;
	}
}
