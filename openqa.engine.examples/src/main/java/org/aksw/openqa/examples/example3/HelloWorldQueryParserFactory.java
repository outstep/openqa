package org.aksw.openqa.examples.example3;

import java.util.Map;

import org.aksw.openqa.component.answerformulation.AbstractQueryParserFactory;
import org.aksw.openqa.component.answerformulation.IQueryParser;

public class HelloWorldQueryParserFactory extends AbstractQueryParserFactory {

	@Override
	public IQueryParser create(Map<String, Object> params) {		
		return create(HelloWorldQueryParser.class, params);
	}

}
