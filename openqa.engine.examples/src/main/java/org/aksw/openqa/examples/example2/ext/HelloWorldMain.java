package org.aksw.openqa.examples.example2.ext;

import java.util.ArrayList;
import java.util.List;

import org.aksw.openqa.AnswerFormulation;
import org.aksw.openqa.SequentialStage;
import org.aksw.openqa.Stage;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.examples.example2.HelloWorldQueryParser;
import org.aksw.openqa.examples.example2.HelloWorldRetriever;
import org.aksw.openqa.main.QueryResult;
import org.aksw.openqa.manager.plugin.PluginManager;

public class HelloWorldMain {

	/**
	 * This Example is an extension of Example 2 and shows how to print the stack trace 
	 * or the result in JSON format.
	 * 
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		String question = "";
		if(args.length > 0) {
			question  = args[0];
		}
		
		System.out.println("You:\t" + question);
		   	    	
    	HelloWorldQueryParser interpreter = new HelloWorldQueryParser();
    	HelloWorldRetriever retriever = new HelloWorldRetriever();
    	
    	PluginManager pluginManager = new PluginManager();
    	pluginManager.register(interpreter);
    	pluginManager.register(retriever);
    	pluginManager.setActive(true, interpreter.getId());
    	pluginManager.setActive(true, retriever.getId());
    	
    	// pipeline instantiation    	
    	List<Stage> stages = new ArrayList<Stage>();
    	Stage stageInterpreter = new SequentialStage();
    	List<String> interpreterComponentIDs = new ArrayList<String>();
    	interpreterComponentIDs.add(interpreter.getId());
    	stageInterpreter.setComponentIDs(interpreterComponentIDs);
    	stages.add(stageInterpreter);
    	
    	Stage stageRetriever = new SequentialStage();
    	List<String> retrieverComponentIDs = new ArrayList<String>();
    	retrieverComponentIDs.add(retriever.getId());
    	stageRetriever.setComponentIDs(retrieverComponentIDs);
    	stages.add(stageRetriever);
    	
    	AnswerFormulation queryProcessor = new AnswerFormulation();
    	
    	QueryResult result;
		result = queryProcessor.process(question, pluginManager, stages);
		IResultMapList<? extends IResultMap> output = result.getOutput();
		
		System.out.println("openQA:\t");
		
		// printing stack trace		
		
		System.out.println("");
		System.out.println("Checking stack trace...");
		System.out.println("");
		System.out.println();
		output.printStackTrace();
		
		// printing JSON object
		
		System.out.println("");
		System.out.println("Printing result in a JSON format...");
		System.out.println("");
		System.out.println(output.toJSON());
	}

}
